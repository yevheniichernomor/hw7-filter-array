/* Відповіді:

1.  Метод forEach перебирає кожний елемент масиву та запускає задану нами функцію для кожного з цього елементів.

2.  Видалити масив можна такими методами: arr.splice(0,arr.length) чи arr.length = 0;
    
3.  Перевірити, чи являється змінна масивом можно за допомогою методу arr.isArray(). Наприклад arr.isArray([1, 2, 'Hello world']) виведе true.
*/


function filterBy(typeofData, ...data){
    let array = [...data];
    
    if (typeofData == 'string') {
        let filter = array.filter(function(element) {
           return (typeof element !== 'string');
        });
        return filter;
    }

    else if (typeofData == 'number') {
        let filter = array.filter(function(element) {
            return (typeof element !== 'number');
        });
        return filter;
    } 

    else if (typeofData == 'null') {
        let filter = array.filter(function(element) {
            return (element !== null);
        });
        return filter;
    } 

    else if (typeofData == 'boolean') {
        let filter = array.filter(function(element) {
            return (typeof element !== 'boolean');
        });
        return filter;
    }

    else if (typeofData == 'undefined') {
        let filter = array.filter(function(element) {
            return (typeof element !== 'undefined');
        });
        return filter;
    }
    
    else if (typeofData == 'object') {
        let filter = array.filter(function(element) {
           return (typeof element !== 'object');
        });
        return filter;
    }

    return array;
}

let res = filterBy('string', 1, true, 2, 28, -10, null, {firstName: 'Oleh'}, false, 'world', undefined, 'hello', [2 , 3], undefined, null, true);

console.log(res);